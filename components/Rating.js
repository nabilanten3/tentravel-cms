import { Button, message, Modal, Table } from "antd";
import React, { useState } from "react";
import { BsThreeDots } from "react-icons/bs";
import { http } from "../utils/http";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { distributorRepository } from "../repository/distributor";
import { MdDeleteOutline } from "react-icons/md";

const Rating = () => {
  const router = useRouter();
  const [totalPage, setTotalPage] = useState();
  const [pagePagination, setPagePagination] = useState(1);

  const { data: dataDistributor } =
    distributorRepository.hooks.getDistributor(pagePagination);

  console.log(dataDistributor?.data, "dataDistributor");

  const onDeleteReview = (record) => {
    Modal.confirm({
      title: "Anda Yakin?",
      content: "Hapus Distributor",
      centered: true,
      cancelText: "Batal",
      okText: "Yakin",
      okType: "danger",
      onOk: async () => {
        try {
          await http.del(
            distributorRepository.url.detailDistributor(record?.id)
          );
          message.success("Berhasil Menghapus Distributor");
          await mutate(distributorRepository.url.distributor(pagePagination));
        } catch (error) {
          console.log(error.message, "error delete");
          message.error("Gagal Menghapus Distributor");
        }
      },
    });
  };

  const columns = [
    {
      title: "Nama Distributor",
      dataIndex: "name",
    },
    {
      title: "Nomor Telepon",
      dataIndex: "phone",
    },
    {
      title: "Alamat",
      dataIndex: "address",
    },
    {
      align: "center",
      title: "Actions",
      width: 150,
      render: (record) => {
        return (
          <div className="flex items-center justify-center gap-x-2">
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <BsThreeDots
                onClick={() => {
                  router.push(`/distributor/${record.id}`);
                }}
                className="text-xl text-gray-400"
              />
            </div>
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <MdDeleteOutline
                onClick={() => {
                  onDeleteReview(record);
                }}
                className="text-xl text-red-500"
              />
            </div>
          </div>
        );
      },
    },
  ];

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            Data Distributor
          </div>
          <div className="flex items-center space-x-4">
            <button
              onClick={() =>
                router.push({
                  pathname: "/distributor/[id]",
                  query: { id: "create" },
                })
              }
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
            >
              Tambah Distributor
            </button>
          </div>
        </div>
        <hr className="border-gray-200" />
        <div className="rounded-sm shadow-sm bg-softWhite">
          <Table
            columns={columns}
            dataSource={dataDistributor?.data}
            pagination={{
              current: pagePagination,
              className: "px-4",
              pageSize: 10,
              total: totalPage,
              onChange(current) {
                setPagePagination(current);
              },
            }}
          ></Table>
        </div>
      </div>
    </div>
  );
};

export default Rating;
