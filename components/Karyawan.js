import { Button, message, Modal, Table } from "antd";
import React, { useState } from "react";
import { http } from "../utils/http";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { MdDeleteOutline } from "react-icons/md";
import { karyawanRepository } from "../repository/karyawan";
import { BiEdit } from "react-icons/bi";

const EmployeeComponent = () => {
  const router = useRouter();

  const { data: dataKaryawan } = karyawanRepository.hooks.usekaryawan();

  console.log(dataKaryawan?.data, "dataKaryawan");

  const onDeleteReview = (record) => {
    Modal.confirm({
      title: "Anda Yakin?",
      content: "Hapus Karyawan",
      centered: true,
      cancelText: "Batal",
      okText: "Yakin",
      okType: "danger",
      onOk: async () => {
        try {
          await http.del(karyawanRepository.url.detailkaryawan(record?.id));
          message.success("Berhasil Menghapus Karyawan");
          await mutate(karyawanRepository.url.karyawan());
        } catch (error) {
          console.log(error.message, "error delete");
          message.error("Gagal Menghapus Karyawan");
        }
      },
    });
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Phone",
      dataIndex: "phone",
    },

    {
      title: "Position",
      dataIndex: "position",
    },
    {
      title: "Address",
      dataIndex: "address",
    },
    {
      align: "center",
      title: "Actions",
      width: 150,
      render: (record) => {
        return (
          <div className="flex items-center justify-center gap-x-2">
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <BiEdit
                onClick={() => {
                  router.push(`/karyawan/${record.id}`);
                }}
                className="text-xl text-gray-400"
              />
            </div>
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <MdDeleteOutline
                onClick={() => {
                  onDeleteReview(record);
                }}
                className="text-xl text-red-500"
              />
            </div>
          </div>
        );
      },
    },
  ];

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            Data Karyawan
          </div>
          <div className="flex items-center space-x-4">
            <button
              onClick={() =>
                router.push({
                  pathname: "/karyawan/[id]",
                  query: { id: "create" },
                })
              }
              className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
            >
              Buat Karyawan
            </button>
          </div>
        </div>
        <hr className="border-gray-200" />
        <div className="rounded-sm shadow-sm bg-softWhite">
          <Table columns={columns} dataSource={dataKaryawan?.data}></Table>
        </div>
      </div>
    </div>
  );
};

export default EmployeeComponent;
