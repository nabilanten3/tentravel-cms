import { message } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { authentication } from "../utils/authentication";
import { TokenUtil } from "../utils/token";
import ProfileSetting from "./ProfileSetting";

const Navbar = () => {
  const router = useRouter();
  const currentRoute = router.pathname;

  const [level, setLevel] = useState("");

  const handleLogout = () => {
    message.success("Success Logout");
    authentication.clearAccesToken();
  };

  console.log(level, "level");

  const levelMenu = () => {
    let menu;

    switch (level) {
      case "operator":
        menu = (
          <div className="flex flex-col gap-4 p-2 mx-16 text-sm font-medium text-gray-500 md:p-0 md:flex-row md:gap-10">
            <Link href={"/daftar-paket"}>
              <div
                className={`p-2 py-5 cursor-pointer ${
                  currentRoute === "/daftar-paket" ||
                  currentRoute === "/daftar-paket/[id]"
                    ? "border-b-2 border-background/80 transition duration-300"
                    : ""
                }`}
              >
                Daftar Paket
              </div>
            </Link>
            <Link href={"/paket-wisata"}>
              <div
                className={`p-2 py-5 cursor-pointer ${
                  currentRoute === "/paket-wisata" ||
                  currentRoute === "/paket-wisata/[id]"
                    ? "border-b-2 border-background/80 transition duration-300"
                    : ""
                }`}
              >
                Paket Wisata
              </div>
            </Link>
            <Link href={"/reservasi"}>
              <div
                className={`p-2 py-5 cursor-pointer ${
                  currentRoute === "/reservasi" ||
                  currentRoute === "/reservasi/[id]"
                    ? "border-b-2 border-background/80 transition duration-300"
                    : ""
                }`}
              >
                Reservasi
              </div>
            </Link>
          </div>
        );
        break;
      case "admin":
        menu = (
          <div className="flex flex-col gap-4 p-2 mx-16 text-sm font-medium text-gray-500 md:p-0 md:flex-row md:gap-10">
            <Link href={"/karyawan"}>
              <div
                className={`p-2 py-5 cursor-pointer ${
                  currentRoute === "/karyawan" ||
                  currentRoute === "/karyawan/[id]"
                    ? "border-b-2 border-background/80 transition duration-300"
                    : ""
                }`}
              >
                Karyawan
              </div>
            </Link>
            <Link href={"/user"}>
              <div
                className={`p-2 py-5 cursor-pointer ${
                  currentRoute === "/user" || currentRoute === "/user/[id]"
                    ? "border-b-2 border-background/80 transition duration-300"
                    : ""
                }`}
              >
                Users
              </div>
            </Link>
          </div>
        );
        break;
      case "pemilik":
        menu = (
          <div className="flex flex-col gap-4 p-2 mx-16 text-sm font-medium text-gray-500 md:p-0 md:flex-row md:gap-10">
            <Link href={"/laporan"}>
              <div
                className={`p-2 py-5 cursor-pointer ${
                  currentRoute === "/laporan"
                    ? "border-b-2 border-background/80 transition duration-300"
                    : ""
                }`}
              >
                Laporan
              </div>
            </Link>
          </div>
        );
        break;
      default:
        break;
    }
    return menu;
  };

  return (
    <nav className="fixed top-0 z-20 w-full shadow-lg shadow-background/5 bg-softWhite">
      <div className="flex flex-wrap items-center justify-between p-6 mx-auto sm:px-10 sm:py-6">
        <div className="flex items-center gap-x-12">
          <Link href={"/"}>
            <div className="flex items-center text-2xl font-bold cursor-pointer text-background">
              TENTRAVEL
            </div>
          </Link>
        </div>
        <div className="flex items-center gap-x-10">
          {TokenUtil.access_token ? (
            <ProfileSetting handleLogout={handleLogout} setLevel={setLevel} />
          ) : (
            <Link href={"/auth/login"}>
              <button className="px-3 py-1.5 font-bold uppercase transition duration-300 border-2 rounded-md cursor-pointer text-background border-background">
                sign in
              </button>
            </Link>
          )}
        </div>
      </div>
      <hr className="mx-10 border-gray-200" />
      {levelMenu()}
    </nav>
  );
};

export default Navbar;
