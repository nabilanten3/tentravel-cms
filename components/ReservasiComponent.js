import { Image, message, Modal, Table } from "antd";
import React from "react";
import { http } from "../utils/http";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { MdDeleteOutline } from "react-icons/md";
import { BiEdit } from "react-icons/bi";
import { reservasiRepository } from "../repository/reservasi";
import dayjs from "dayjs";

const ReservasiComponent = () => {
  const router = useRouter();

  const { data: dataReservasi } = reservasiRepository.hooks.usereservasi();

  console.log(dataReservasi?.data, "dataReservasi");

  const onDeleteReview = (record) => {
    Modal.confirm({
      title: "Anda Yakin?",
      content: "Hapus Reservasi",
      centered: true,
      cancelText: "Batal",
      okText: "Yakin",
      okType: "danger",
      onOk: async () => {
        try {
          await http.del(
            reservasiRepository.url.detailreservasiDelete(record?.id)
          );
          message.success("Berhasil Menghapus Reservasi");
          await mutate(reservasiRepository.url.reservasi());
        } catch (error) {
          console.log(error.message, "error delete");
          message.error("Gagal Menghapus Reservasi");
        }
      },
    });
  };

  const columns = [
    {
      title: "Tanggal Reservasi",
      dataIndex: "date",
      render: (text) => dayjs(text).format("DD-MM-YYYY"),
    },
    {
      title: "Nama Paket",
      dataIndex: "paket",
      render: (text) => text?.name,
    },
    {
      title: "nama Pelanggan",
      dataIndex: "pelanggan",
      render: (text) => text?.name,
    },

    {
      title: "Harga",
      dataIndex: "price",
    },
    {
      title: "Status",
      dataIndex: "status",
    },

    {
      align: "center",
      title: "Actions",
      width: 150,
      render: (record) => {
        return (
          <div className="flex items-center justify-center gap-x-2">
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <BiEdit
                onClick={() => {
                  router.push(`/reservasi/${record.id}`);
                }}
                className="text-xl text-gray-400"
              />
            </div>
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <MdDeleteOutline
                onClick={() => {
                  onDeleteReview(record);
                }}
                className="text-xl text-red-500"
              />
            </div>
          </div>
        );
      },
    },
  ];

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            Data Reservasi
          </div>
          <div className="flex items-center space-x-4">
            <button
              onClick={() =>
                router.push({
                  pathname: "/reservasi/[id]",
                  query: { id: "create" },
                })
              }
              className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
            >
              Buat Reservasi
            </button>
          </div>
        </div>
        <hr className="border-gray-200" />
        <div className="rounded-sm shadow-sm bg-softWhite">
          <Table columns={columns} dataSource={dataReservasi?.data}></Table>
        </div>
      </div>
    </div>
  );
};

export default ReservasiComponent;
