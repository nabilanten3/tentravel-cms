import { Image, message, Modal, Table } from "antd";
import React from "react";
import { http } from "../utils/http";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { MdDeleteOutline } from "react-icons/md";
import { BiEdit } from "react-icons/bi";
import { paketWisataRepository } from "../repository/paket-wisata";

const PaketWisataComponent = () => {
  const router = useRouter();

  const { data: dataPaketWisata } =
    paketWisataRepository.hooks.usepaketWisata();

  console.log(dataPaketWisata?.data, "dataPaketWisata");

  const onDeleteReview = (record) => {
    Modal.confirm({
      title: "Anda Yakin?",
      content: "Hapus Paket Wisata",
      centered: true,
      cancelText: "Batal",
      okText: "Yakin",
      okType: "danger",
      onOk: async () => {
        try {
          await http.del(
            paketWisataRepository.url.detailpaketWisata(record?.id)
          );
          message.success("Berhasil Menghapus Paket Wisata");
          await mutate(paketWisataRepository.url.paketWisata());
        } catch (error) {
          console.log(error.message, "error delete");
          message.error("Gagal Menghapus Paket Wisata");
        }
      },
    });
  };

  const columns = [
    {
      title: "Foto",
      dataIndex: "image",
      render: (text) => (
        <Image
          src={`http://localhost:3222/file/${text}`}
          width={50}
          height={50}
          alt="Foto Paket Wisata"
          preview={false}
        />
      ),
    },
    {
      title: "Nama Paket",
      dataIndex: "name",
    },
    {
      title: "Rencana Perjalanan",
      dataIndex: "itinerary",
    },

    {
      title: "Fasilitas",
      dataIndex: "facility",
    },
    {
      title: "Deskripsi",
      dataIndex: "description",
    },
    {
      align: "center",
      title: "Aksi",
      width: 150,
      render: (record) => {
        return (
          <div className="flex items-center justify-center gap-x-2">
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <BiEdit
                onClick={() => {
                  router.push(`/paket-wisata/${record.id}`);
                }}
                className="text-xl text-gray-400"
              />
            </div>
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <MdDeleteOutline
                onClick={() => {
                  onDeleteReview(record);
                }}
                className="text-xl text-red-500"
              />
            </div>
          </div>
        );
      },
    },
  ];

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            Data Paket Wisata
          </div>
          <div className="flex items-center space-x-4">
            <button
              onClick={() =>
                router.push({
                  pathname: "/paket-wisata/[id]",
                  query: { id: "create" },
                })
              }
              className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
            >
              Buat Paket Wisata
            </button>
          </div>
        </div>
        <hr className="border-gray-200" />
        <div className="rounded-sm shadow-sm bg-softWhite">
          <Table columns={columns} dataSource={dataPaketWisata?.data}></Table>
        </div>
      </div>
    </div>
  );
};

export default PaketWisataComponent;
