import { Button, message, Modal, Table } from "antd";
import React, { useState } from "react";
import { BsThreeDots } from "react-icons/bs";
import { http } from "../utils/http";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { MdDeleteOutline } from "react-icons/md";
import moment from "moment/moment";
import { pembelianRepository } from "../repository/pembelian";

const PembelianComponent = () => {
  const router = useRouter();

  const { data: dataPembelian } = pembelianRepository.hooks.usepembelian();

  console.log(dataPembelian?.data, "dataPembelian");

  const onDeleteReview = (record) => {
    Modal.confirm({
      title: "Anda Yakin?",
      content: "Hapus Penjualan",
      centered: true,
      cancelText: "Batal",
      okText: "Yakin",
      okType: "danger",
      onOk: async () => {
        try {
          await http.del(pembelianRepository.url.detailpembelian(record?.id));
          message.success("Berhasil Menghapus Pembelian");
          await mutate(pembelianRepository.url.pembelian());
        } catch (error) {
          console.log(error.message, "error delete");
          message.error("Gagal Menghapus Pembelian");
        }
      },
    });
  };

  const columns = [
    {
      title: "Tangal Beli",
      dataIndex: "tanggal",
      render: (text) => moment(text).format("DD-MM-YYYY"),
    },
    {
      title: "No. Nota",
      dataIndex: "nota_number",
    },
    {
      title: "Distributor",
      dataIndex: "distributor",
      render: (text) => text.name,
    },
    {
      title: "Barang",
      dataIndex: "product",
      render: (text) => text.name,
    },
    {
      title: "Harga",
      dataIndex: "price",
    },
    {
      title: "Jumlah",
      dataIndex: "quantity",
    },
    {
      title: "Total",
      dataIndex: "total",
    },
    {
      align: "center",
      title: "Actions",
      width: 150,
      render: (record) => {
        return (
          <div className="flex items-center justify-center gap-x-2">
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <BsThreeDots
                onClick={() => {
                  router.push(`/pembelian/${record.id}`);
                }}
                className="text-xl text-gray-400"
              />
            </div>
            <div className="flex items-center px-4 py-2 bg-gray-100 rounded-md cursor-pointer">
              <MdDeleteOutline
                onClick={() => {
                  onDeleteReview(record);
                }}
                className="text-xl text-red-500"
              />
            </div>
          </div>
        );
      },
    },
  ];

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            Data Pembelian
          </div>
          <div className="flex items-center space-x-4">
            <button
              onClick={() =>
                router.push({
                  pathname: "/pembelian/[id]",
                  query: { id: "create" },
                })
              }
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
            >
              Tambah Penjualan
            </button>
          </div>
        </div>
        <hr className="border-gray-200" />
        <div className="rounded-sm shadow-sm bg-softWhite">
          <Table columns={columns} dataSource={dataPembelian?.data}></Table>
        </div>
      </div>
    </div>
  );
};

export default PembelianComponent;
