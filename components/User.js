import { message, Modal, Table } from "antd";
import React from "react";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { userRepository } from "../repository/user";
import { http } from "../utils/http";
import { mutate } from "swr";
import { useRouter } from "next/router";

const User = () => {
  const router = useRouter();

  const { data: dataUser } = userRepository.hooks.getUser();

  console.log(dataUser, "dataUser");

  const onDeleteUser = (record) => {
    Modal.confirm({
      title: "Anda Yakin?",
      content: "Ingin Menghapus user ini",
      centered: true,
      okText: "Hapus",
      cancelText: "Batal",
      okType: "danger",
      onOk: async () => {
        try {
          await http.del(userRepository.url.detailUser(record?.id));
          mutate(userRepository.url.user());
          message.success("Berhasil Menghapus User");
        } catch (error) {
          console.log(error.message, "error delete");
          message.error("Gagal Menghapus User");
        }
      },
    });
  };

  const columns = [
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },

    {
      title: "Level",
      align: "center",
      dataIndex: "level",
      key: "role",
    },
    {
      align: "center",
      title: "Aksi",
      render: (record) => {
        return (
          <div className="text-center">
            <EditOutlined
              onClick={() =>
                router.push({
                  pathname: "/user/[id]",
                  query: { id: record?.id },
                })
              }
            />
            <DeleteOutlined
              onClick={() => {
                onDeleteUser(record);
              }}
              style={{ color: "maroon", marginLeft: 14 }}
            />
          </div>
        );
      },
    },
  ];

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            Data User
          </div>
          <button
            onClick={() =>
              router.push({
                pathname: "/user/[id]",
                query: { id: "create" },
              })
            }
            className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
          >
            Buat User
          </button>
        </div>
        <hr className="border-gray-200" />
        <div className="rounded-sm shadow-sm bg-softWhite">
          <Table
            bordered={true}
            columns={columns}
            dataSource={dataUser?.data}
          ></Table>
        </div>
      </div>
    </div>
  );
};

export default User;
