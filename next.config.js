/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  eslint: {
    ignoreDuringBuilds: true,
  },
  images: {
    remotePatterns: [{
      protocol: "http",
      hostname: "127.0.0.1"
    }],
    domains: ["source.unsplash.com", "127.0.0.1"],
  },
};

module.exports = nextConfig;