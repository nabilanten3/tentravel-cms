import { Form, Input, InputNumber, message, Select, Space } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { daftarPaketRepository } from "../../../repository/daftar-paket";
import { paketWisataRepository } from "../../../repository/paket-wisata";
const SuperAgent = require("superagent");

const DetailDaftarPaket = () => {
  const [from] = Form.useForm();
  const router = useRouter();

  const { id } = router.query;

  const { data: detailDaftarPaket } =
    daftarPaketRepository.hooks.useDetaildaftarPaket(id);

  const { data: dataPaketWisata } =
    paketWisataRepository.hooks.usepaketWisata();

  const [mode, setMode] = useState("");

  useEffect(() => {
    if (detailDaftarPaket) {
      const initData = () => {
        from.setFieldsValue({
          paketWisataId: detailDaftarPaket?.data?.paket?.id,
          name: detailDaftarPaket?.data?.name,
          participants: detailDaftarPaket?.data?.participants,
          price: detailDaftarPaket?.data?.price,
        });
      };
      initData();
    }
  });

  useEffect(() => {
    if (detailDaftarPaket) {
      setMode("edit");
    } else {
      setMode("create");
    }
  });

  const handleSubmit = async () => {
    const data = await from.validateFields();

    if (mode === "edit") {
      try {
        await SuperAgent.put(
          appConfig.apiUrl + daftarPaketRepository.url.detaildaftarPaket(id)
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Mengubah Daftar Paket");
        router.push("/daftar-paket");
      } catch (e) {
        message.error("Gagal Mengubah Daftar Paket");
      }
    } else {
      try {
        await SuperAgent.post(
          appConfig.apiUrl + daftarPaketRepository.url.daftarPaket()
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Menambah Daftar Paket");
        router.push("/daftar-paket");
      } catch (e) {
        message.error("Gagal Membuat Daftar Paket");
      }
    }
  };

  return (
    <div className="p-10 bg-gray-100 mt-56 max-w-2xl mx-auto">
      <Form layout="vertical" form={from}>
        <Form.Item name={"paketWisataId"} label={"Nama Paket Wisata"}>
          <Select>
            {dataPaketWisata?.data?.map((data, index) => {
              return (
                <Select.Option key={index} value={data?.id}>
                  {data?.name}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name={"name"} label={"Nama"}>
          <Input />
        </Form.Item>
        <Form.Item name={"participants"} label={"participants"}>
          <InputNumber />
        </Form.Item>
        <Form.Item name={"price"} label={"price"}>
          <InputNumber />
        </Form.Item>
        <Form.Item>
          <Space size={20} className={"text-end justify-end flex"}>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-softWhite text-background"
              onClick={() => router.back()}
            >
              Batal
            </button>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
              onClick={handleSubmit}
            >
              {mode === "edit" ? "Simpan" : "Tambah"}
            </button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
};

export default DetailDaftarPaket;

DetailDaftarPaket.getLayout = (page) => <CMSLayout children={page} />;
