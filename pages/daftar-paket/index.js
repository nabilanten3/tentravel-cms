import React from "react";
import DaftartPaketComponent from "../../components/DaftarPaketComponent";
import CMSLayout from "../../layouts/CMSLayout";

const DaftarPaket = () => {
  return (
    <div>
      <DaftartPaketComponent />
    </div>
  );
};

export default DaftarPaket;

DaftarPaket.getLayout = (page) => <CMSLayout children={page} />;
