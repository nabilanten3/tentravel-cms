import React from "react";
import ReservasiComponent from "../../components/ReservasiComponent";
import CMSLayout from "../../layouts/CMSLayout";

const Reservasi = () => {
  return (
    <div>
      <ReservasiComponent />
    </div>
  );
};

export default Reservasi;

Reservasi.getLayout = (page) => <CMSLayout children={page} />;
