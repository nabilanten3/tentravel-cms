import {
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Select,
  Space,
} from "antd";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { daftarPaketRepository } from "../../../repository/daftar-paket";
import { pelangganRepository } from "../../../repository/pelanggan";
import { reservasiRepository } from "../../../repository/reservasi";
const SuperAgent = require("superagent");

const DetailReservasi = () => {
  const [from] = Form.useForm();
  const router = useRouter();

  const { id } = router.query;

  const { data: detailreservasi } =
    reservasiRepository.hooks.useDetailreservasi(id);
  const { data: dataUser } = pelangganRepository.hooks.usepelanggan();
  const { data: dataPaket } = daftarPaketRepository.hooks.usedaftarPaket();

  const [mode, setMode] = useState("");

  useEffect(() => {
    if (detailreservasi) {
      const initData = () => {
        from.setFieldsValue({
          pelangganId: detailreservasi?.data?.pelanggan?.id,
          daftarPaketId: detailreservasi?.data?.paket?.id,
          reservationDate: dayjs(detailreservasi?.data?.date),
        });
      };
      initData();
    }
  });

  useEffect(() => {
    if (detailreservasi) {
      setMode("edit");
    } else {
      setMode("create");
    }
  });

  const handleSubmit = async () => {
    const values = await from.validateFields();

    const data = {
      image: [
        "https://images.unsplash.com/photo-1558611913-d707111c702e?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=500&ixid=MnwxfDB8MXxyYW5kb218MHx8cGFyaXN8fHx8fHwxNjgwNTk3NjQy&ixlib=rb-4.0.3&q=80&utm_campaign=api-credit&utm_medium=referral&utm_source=unsplash_source&w=500",
      ],
      ...values,
    };

    if (mode === "edit") {
      try {
        await SuperAgent.put(
          appConfig.apiUrl + reservasiRepository.url.detailreservasi(id)
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Mengubah Reservasi");
        router.push("/reservasi");
      } catch (e) {
        message.error("Gagal Mengubah Reservasi");
      }
    } else {
      try {
        await SuperAgent.post(
          appConfig.apiUrl + reservasiRepository.url.reservasi()
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Menambah Reservasi");
        router.push("/reservasi");
      } catch (e) {
        message.error("Gagal Membuat Reservasi");
      }
    }
  };

  return (
    <div className="p-10 bg-gray-100 mt-56 max-w-2xl mx-auto">
      <Form layout="vertical" form={from}>
        <Form.Item name={"pelangganId"} label={"Nama"}>
          <Select disabled={mode === "edit" ? true : false}>
            {dataUser?.data?.map((data, index) => {
              return (
                <Select.Option value={data?.id}>{data?.name}</Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name={"daftarPaketId"} label={"Daftar Paket"}>
          <Select disabled={mode === "edit" ? true : false}>
            {dataPaket?.data?.map((data, index) => {
              return (
                <Select.Option value={data?.id}>{data?.name}</Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name={"reservationDate"} label={"reservationDate"}>
          <DatePicker disabled={mode === "edit" ? true : false} />
        </Form.Item>
        <Form.Item>
          <Space size={20} className={"text-end justify-end flex"}>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-softWhite text-background"
              onClick={() => router.back()}
            >
              Batal
            </button>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
              onClick={handleSubmit}
            >
              {mode === "edit" ? "Simpan" : "Tambah"}
            </button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
};

export default DetailReservasi;

DetailReservasi.getLayout = (page) => <CMSLayout children={page} />;
