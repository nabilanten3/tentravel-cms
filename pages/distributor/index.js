import React from "react";
import Rating from "../../components/Rating";
import CMSLayout from "../../layouts/CMSLayout";

const distributor = () => {
  return (
    <div>
      <Rating />
    </div>
  );
};

export default distributor;

distributor.getLayout = (page) => <CMSLayout children={page} />;
