import { Form, Input, message, Space } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { distributorRepository } from "../../../repository/distributor";
const SuperAgent = require("superagent");

const DetailDistibutor = () => {
  const [from] = Form.useForm();
  const router = useRouter();

  const { id } = router.query;

  const { data: detailDistributor } =
    distributorRepository.hooks.getDetailDistributor(id);

  const [mode, setMode] = useState("");

  useEffect(() => {
    if (detailDistributor) {
      const initData = () => {
        from.setFieldsValue({
          name: detailDistributor?.data?.name,
          phone: detailDistributor?.data?.phone,
          address: detailDistributor?.data?.address,
        });
      };
      initData();
    }
  });

  useEffect(() => {
    if (detailDistributor) {
      setMode("edit");
    } else {
      setMode("create");
    }
  });

  const handleSubmit = async () => {
    const data = await from.validateFields();
    if (mode === "edit") {
      try {
        await SuperAgent.put(
          appConfig.apiUrl + distributorRepository.url.detailDistributor(id)
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Mengubah Distributor");
        router.push("/distributor");
      } catch (e) {
        message.error("Gagal Mengubah Distributor");
      }
    } else {
      try {
        await SuperAgent.post(appConfig.apiUrl + "/distributor")
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Menambah Distributor");
        router.push("/distributor");
      } catch (e) {
        message.error("Gagal Membuat Distributor");
      }
    }
  };

  return (
    <div className="p-10 bg-gray-100 mt-56 max-w-2xl mx-auto">
      <Form layout="vertical" form={from}>
        <Form.Item name={"name"} label={"Nama Distributor"}>
          <Input />
        </Form.Item>
        <Form.Item name={"phone"} label={"Nomor Telepon"}>
          <Input />
        </Form.Item>
        <Form.Item name={"address"} label={"Alamat Distributor"}>
          <Input />
        </Form.Item>
        <Form.Item>
          <Space size={20} className={"text-end justify-end flex"}>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-softWhite text-background"
              onClick={() => router.back()}
            >
              Batal
            </button>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
              onClick={handleSubmit}
            >
              {mode === "edit" ? "Simpan" : "Tambah"}
            </button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
};

export default DetailDistibutor;

DetailDistibutor.getLayout = (page) => <CMSLayout children={page} />;
