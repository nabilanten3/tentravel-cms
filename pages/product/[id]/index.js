import { message, Select } from "antd";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { mutate } from "swr";
import UploadImage from "../../../components/UploadImage";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { productRepository } from "../../../repository/product";

const SuperAgent = require("superagent");
const { Option } = Select;

const FormProduct = () => {
  const router = useRouter();
  const { id } = router.query;

  const [mode, setMode] = useState("create");
  const [image, setImage] = useState([]);
  const [categoryId, setCategoryId] = useState();
  const [type, setType] = useState();

  const { data: detailproduct } = productRepository.hooks.getDetailproduct(id);
  const product = detailproduct?.data;

  console.log(product, "product");

  const result = [];
  image.map((data) => {
    result.push(data?.response?.data?.filename);
  });

  console.log(result);
  useEffect(() => {
    detailproduct ? setMode("edit") : setMode("create");
  }, [detailproduct]);

  // Form Control
  const nameRef = useRef();
  const priceRef = useRef();
  const merkRef = useRef();
  const stockRef = useRef();
  const jenisRef = useRef();
  const satuanRef = useRef();
  const golonganRef = useRef();
  const kemasanRef = useRef();

  const onSubmitForm = async (event) => {
    event.preventDefault();
    const oldData = product;
    try {
      if (mode === "create") {
        const data = {
          name: nameRef.current.value,
          price: Number(priceRef.current.value),
          merk: merkRef.current.value,
          stock: Number(stockRef.current.value),
          image: result,
          jenis: jenisRef.current.value,
          satuan: satuanRef.current.value,
          golongan: golonganRef.current.value,
          kemasan: kemasanRef.current.value,
        };
        console.log(data);
        await SuperAgent.post(appConfig.apiUrl + "/products")
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          )
          .then((res) => console.log(res));
        await mutate(productRepository.url.product());
      } else {
        console.log(satuanRef.current.value, "satuanRef.current.value");
        const data = {
          name: nameRef.current.value ? nameRef.current.value : oldData.name,
          price: Number(
            priceRef.current.value ? priceRef.current.value : oldData.price
          ),
          merk: merkRef.current.value ? merkRef.current.value : oldData.merk,
          stock: Number(
            stockRef.current.value ? stockRef.current.value : oldData.stok
          ),
          // image: image ? image : oldData.image,
          jenis: jenisRef.current.value
            ? jenisRef.current.value
            : oldData?.jenis,
          satuan: satuanRef.current.value
            ? satuanRef.current.value
            : oldData?.satuan,
          golongan: golonganRef.current.value
            ? golonganRef.current.value
            : oldData?.golongan,
          kemasan: kemasanRef.current.value
            ? kemasanRef.current.value
            : oldData?.kemasan,
        };
        await SuperAgent.put(appConfig.apiUrl + `/products/${id}`)
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          )
          .then((res) => console.log(res));
        await mutate(productRepository.url.product());
      }
      message
        .success(
          `Successfully ${mode === "create" ? "Created" : "Updated"} Product`
        )
        .then(router.push("/product"));
    } catch (e) {
      message.error("Failed to Create Product");
      console.log(e.message);
    }
  };

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            {`${id === "create" ? "Create" : "Edit"} Products`}
          </div>
          <button
            onClick={() => router.back()}
            className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
          >
            Back
          </button>
        </div>
        <hr className="border-gray-200" />
        <div className="p-10 bg-white/80">
          <form onSubmit={onSubmitForm}>
            <div className="grid grid-cols-2 2xl:w-[90%]">
              <div className="flex items-start justify-end p-4">
                <div
                  className={`w-full h-full ${
                    mode === "edit"
                      ? "xl:w-[98%] 2xl:w-[77%] h-auto"
                      : "xl:w-[98%] 2xl:w-[77%] h-auto"
                  } p-4 bg-gray-100 lg:p-6 xl:p-8`}
                >
                  {mode === "edit" ? (
                    <div className="w-full overflow-hidden rounded-sm">
                      <div className="flex flex-col-reverse gap-2 xl:gap-5 md:flex-row">
                        <div className="flex flex-row items-center md:flex-col">
                          <div
                            className="flex flex-row items-center gap-2 p-2 overflow-scroll max-w-screen max-h-[460px] md:flex-col bg-gray-100 scroll-smooth scrollbar-hide"
                            id="slider"
                          >
                            <div className="md:w-[40px] md:h-[40px] lg:w-[60px] lg:h-[60px] xl:w-[100px] xl:h-[100px]">
                              <Image
                                src={`http://127.0.0.1:3222/file/${product?.image[1]}`}
                                preview={false}
                                width={500}
                                height={500}
                                className="w-full h-full"
                                alt="Product Image"
                              />
                            </div>

                            <div className="md:w-[40px] md:h-[40px] lg:w-[60px] lg:h-[60px] xl:w-[100px] xl:h-[100px]">
                              <Image
                                src={`http://127.0.0.1:3222/file/${product?.image[2]}`}
                                preview={false}
                                width={500}
                                height={500}
                                className="w-full h-full"
                                alt="Product Image"
                              />
                            </div>

                            <div className="md:w-[40px] md:h-[40px] lg:w-[60px] lg:h-[60px] xl:w-[100px] xl:h-[100px]">
                              <Image
                                src={`http://127.0.0.1:3222/file/${product?.image[3]}`}
                                preview={false}
                                width={500}
                                height={500}
                                className="w-full h-full"
                                alt="Product Image"
                              />
                            </div>

                            <div className="md:w-[40px] md:h-[40px] lg:w-[60px] lg:h-[60px] xl:w-[100px] xl:h-[100px]">
                              <Image
                                src={`http://127.0.0.1:3222/file/${product?.image[4]}`}
                                preview={false}
                                width={500}
                                height={500}
                                className="w-full h-full"
                                alt="Product Image"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="w-full h-full">
                          <Image
                            src={`http://127.0.0.1:3222/file/${product?.image[0]}`}
                            preview={false}
                            width={500}
                            height={500}
                            className="w-full max-h-[475px]"
                            alt="Product Image"
                          />
                        </div>
                      </div>
                    </div>
                  ) : (
                    <UploadImage setImage={setImage} />
                  )}
                </div>
              </div>
              <div className="p-4">
                <label className="text-sm font-medium text-gray-500">
                  Name
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={nameRef}
                    defaultValue={product?.name}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Price
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={priceRef}
                    defaultValue={product?.price}
                    type="number"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Merk
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={merkRef}
                    defaultValue={product?.merk}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Stock
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={stockRef}
                    defaultValue={product?.stock}
                    type="number"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Jenis
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={jenisRef}
                    defaultValue={product?.jenis}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Satuan
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={satuanRef}
                    defaultValue={product?.satuan}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Golongan
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={golonganRef}
                    defaultValue={product?.golongan}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Kemasan
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={kemasanRef}
                    defaultValue={product?.kemasan}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
              </div>
            </div>

            <div className="flex items-center justify-end mt-4">
              <button
                type="submit"
                className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
              >
                {`${id === "create" ? "Add New Product" : "Save Change"}`}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default FormProduct;

FormProduct.getLayout = (page) => <CMSLayout children={page} />;
