import React from "react";
import PembelianComponent from "../../components/Pembelian";
import CMSLayout from "../../layouts/CMSLayout";

const Pembelian = () => {
  return (
    <div>
      <PembelianComponent />
    </div>
  );
};

export default Pembelian;

Pembelian.getLayout = (page) => <CMSLayout children={page} />;
