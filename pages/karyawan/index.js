import React from "react";
import KaryawanComponent from "../../components/Karyawan";
import CMSLayout from "../../layouts/CMSLayout";

const Karyawan = () => {
  return (
    <div>
      <KaryawanComponent />
    </div>
  );
};

export default Karyawan;

Karyawan.getLayout = (page) => <CMSLayout children={page} />;
