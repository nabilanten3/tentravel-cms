import {
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Select,
  Space,
} from "antd";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { karyawanRepository } from "../../../repository/karyawan";
import { userRepository } from "../../../repository/user";
const SuperAgent = require("superagent");

const DetailKaryawan = () => {
  const [from] = Form.useForm();
  const router = useRouter();

  const { id } = router.query;

  const { data: detailKaryawan } =
    karyawanRepository.hooks.useDetailkaryawan(id);

  const [mode, setMode] = useState("");

  useEffect(() => {
    if (detailKaryawan) {
      const initData = () => {
        from.setFieldsValue({
          name: detailKaryawan?.data?.name,
          phone: detailKaryawan?.data?.phone,
          address: detailKaryawan?.data?.address,
          position: detailKaryawan?.data?.position,
        });
      };
      initData();
    }
  });

  useEffect(() => {
    if (detailKaryawan) {
      setMode("edit");
    } else {
      setMode("create");
    }
  });

  const handleSubmit = async () => {
    const values = await from.validateFields();
    console.log(values, "values");
    const data = {
      image: "https://source.unsplash.com/random/300x300?avatar",
      ...values,
    };

    if (mode === "edit") {
      try {
        await SuperAgent.put(
          appConfig.apiUrl + karyawanRepository.url.detailkaryawan(id)
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Mengubah karyawan");
        router.push("/karyawan");
      } catch (e) {
        message.error("Gagal Mengubah karyawan");
      }
    } else {
      try {
        await SuperAgent.post(appConfig.apiUrl + "/auth/register")
          .send(values)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Membuat karyawan");
        router.push("/karyawan");
      } catch (e) {
        message.error("Gagal Membuat karyawan");
      }
    }
  };

  return (
    <div className="p-10 bg-gray-100 mt-56 max-w-2xl mx-auto">
      <Form layout="vertical" form={from}>
        <Form.Item name={"name"} label={"Nama"}>
          <Input />
        </Form.Item>
        {mode !== "edit" && (
          <>
            <Form.Item name={"email"} label={"Email"}>
              <Input />
            </Form.Item>
            <Form.Item name={"password"} label={"password"}>
              <Input.Password />
            </Form.Item>
            <Form.Item name={"level"} label={"level"}>
              <Select
                required
                className="w-full h-[45px] rounded-xl "
                options={[
                  {
                    value: "pemilik",
                    label: "pemilik",
                  },
                  {
                    value: "admin",
                    label: "admin",
                  },
                  {
                    value: "operator",
                    label: "operator",
                  },
                  {
                    value: "Pelanggan",
                    label: "pelanggan",
                  },
                ]}
              />
            </Form.Item>
          </>
        )}
        <Form.Item name={"phone"} label={"Nomor Telepon"}>
          <Input type="number" />
        </Form.Item>
        <Form.Item name={"address"} label={"Alamat"}>
          <Input.TextArea />
        </Form.Item>
        {mode === "edit" && (
          <Form.Item name={"position"} label={"Jabatan"}>
            <Input />
          </Form.Item>
        )}

        <Form.Item>
          <Space size={20} className={"text-end justify-end flex"}>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-softWhite text-background"
              onClick={() => router.back()}
            >
              Batal
            </button>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
              onClick={handleSubmit}
            >
              {mode === "edit" ? "Simpan" : "Tambah"}
            </button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
};

export default DetailKaryawan;

DetailKaryawan.getLayout = (page) => <CMSLayout children={page} />;
