import {
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Select,
  Space,
} from "antd";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { penjualanRepository } from "../../../repository/penjualan";
import { productRepository } from "../../../repository/product";
const SuperAgent = require("superagent");

const DetailPenjualan = () => {
  const [from] = Form.useForm();
  const router = useRouter();

  const { id } = router.query;

  const { data: detailPenjualan } =
    penjualanRepository.hooks.useDetailPenjualan(id);
  const { data: dataObat } = productRepository.hooks.getProduct(1, " ");

  const [mode, setMode] = useState("");

  useEffect(() => {
    if (detailPenjualan) {
      const initData = () => {
        from.setFieldsValue({
          productId: detailPenjualan?.data?.product?.id,
          price: detailPenjualan?.data?.price,
          jumlah_jual: detailPenjualan?.data?.jumlah_jual,
          tanggal: dayjs(detailPenjualan?.data?.tanggal),
          expired: dayjs(detailPenjualan?.data?.expired),
          percentage_margin: detailPenjualan?.data?.percentage_margin,
          total: detailPenjualan?.data?.total,
        });
      };
      initData();
    }
  });

  useEffect(() => {
    if (detailPenjualan) {
      setMode("edit");
    } else {
      setMode("create");
    }
  });

  const handleSubmit = async () => {
    const data = await from.validateFields();
    if (mode === "edit") {
      try {
        await SuperAgent.put(
          appConfig.apiUrl + penjualanRepository.url.detailPenjualan(id)
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Mengubah Penjualan");
        router.push("/penjualan");
      } catch (e) {
        message.error("Gagal Mengubah Penjualan");
      }
    } else {
      try {
        await SuperAgent.post(appConfig.apiUrl + "/Penjualan")
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Menambah Penjualan");
        router.push("/penjualan");
      } catch (e) {
        message.error("Gagal Membuat Penjualan");
      }
    }
  };

  return (
    <div className="p-10 bg-gray-100 mt-56 max-w-2xl mx-auto">
      <Form layout="vertical" form={from}>
        <Form.Item name={"productId"} label={"Barang"}>
          <Select>
            {dataObat?.data?.map((data) => {
              return (
                <Select.Option value={data?.id} key={data?.id}>
                  {data?.name}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name={"jumlah_jual"} label={"Jumlah"}>
          <Input type="number" />
        </Form.Item>
        <Form.Item name={"price"} label={"Harga"}>
          <Input type="number" />
        </Form.Item>
        <Form.Item name={"total"} label={"Total"}>
          <Input type="number" />
        </Form.Item>
        <Form.Item name={"percentage_margin"} label={"Persentase Margin"}>
          <InputNumber formatter={(value) => `${value}%`} />
        </Form.Item>
        <Form.Item name={"tanggal"} label={"Tanggal Pembeli"}>
          <DatePicker format={"DD-MM-YYYY HH:MM"} showTime showMinute />
        </Form.Item>
        <Form.Item name={"expired"} label={"Tanggal Kadaluarsa"}>
          <DatePicker format={"DD-MM-YYYY HH:MM"} />
        </Form.Item>
        <Form.Item>
          <Space size={20} className={"text-end justify-end flex"}>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-softWhite text-background"
              onClick={() => router.back()}
            >
              Batal
            </button>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
              onClick={handleSubmit}
            >
              Tambah
            </button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
};

export default DetailPenjualan;

DetailPenjualan.getLayout = (page) => <CMSLayout children={page} />;
