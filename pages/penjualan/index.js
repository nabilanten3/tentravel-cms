import React from "react";
import PenjualanComponent from "../../components/Penjualan";
import CMSLayout from "../../layouts/CMSLayout";

const Penjualan = () => {
  return (
    <div>
      <PenjualanComponent />
    </div>
  );
};

export default Penjualan;

Penjualan.getLayout = (page) => <CMSLayout children={page} />;
