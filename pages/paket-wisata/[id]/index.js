import { Form, Image, Input, InputNumber, message, Space } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { paketWisataRepository } from "../../../repository/paket-wisata";
import UploadImage from "../../../components/UploadImage";
const SuperAgent = require("superagent");

const DetailPaketWisata = () => {
  const [from] = Form.useForm();
  const router = useRouter();

  const { id } = router.query;

  const { data: detailpaketWisata } =
    paketWisataRepository.hooks.useDetailpaketWisata(id);

  const [mode, setMode] = useState("");

  const [image, setImage] = useState([]);
  const [display, setDisplay] = useState("");

  const result = [];
  image?.map((data) => {
    result.push(data?.response?.data?.filename);
  });

  console.log(result, "result");

  useEffect(() => {
    if (detailpaketWisata) {
      const initData = () => {
        from.setFieldsValue({
          name: detailpaketWisata?.data?.name,
          itinerary: detailpaketWisata?.data?.itinerary,
          facility: detailpaketWisata?.data?.facility,
          description: detailpaketWisata?.data?.description,
          discount: detailpaketWisata?.data?.discount,
        });

        setDisplay(detailpaketWisata?.data?.image[0]);
      };
      initData();
    }
  });

  useEffect(() => {
    if (detailpaketWisata) {
      setMode("edit");
    } else {
      setMode("create");
    }
  });

  const handleSubmit = async () => {
    const values = await from.validateFields();

    const data = {
      image: display ? [display] : result,
      ...values,
    };

    if (mode === "edit") {
      try {
        await SuperAgent.put(
          appConfig.apiUrl + paketWisataRepository.url.detailpaketWisata(id)
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Mengubah Paket Wisata");
        router.push("/paket-wisata");
      } catch (e) {
        message.error("Gagal Mengubah Paket Wisata");
      }
    } else {
      try {
        await SuperAgent.post(
          appConfig.apiUrl + paketWisataRepository.url.paketWisata()
        )
          .send(data)
          .set(
            "Authorization",
            "Bearer " + localStorage.getItem("access_token")
          );
        message.success("Berhasil Menambah Paket Wisata");
        router.push("/paket-wisata");
      } catch (e) {
        message.error("Gagal Membuat Paket Wisata");
      }
    }
  };

  return (
    <div className="p-10 bg-gray-100 mt-56 max-w-2xl mx-auto">
      <Form layout="vertical" form={from}>
        <Form.Item name={"name"} label={"Nama"}>
          <Input />
        </Form.Item>
        <Form.Item name={"itinerary"} label={"Rencana Perjalanan"}>
          <Input />
        </Form.Item>
        <Form.Item name={"facility"} label={"Fasilitas"}>
          <Input />
        </Form.Item>
        <Form.Item name={"description"} label={"Deskripsi"}>
          <Input.TextArea />
        </Form.Item>
        <Form.Item name={"discount"} label={"Diskon"}>
          <InputNumber />
        </Form.Item>
        <Form.Item label={"Foto"}>
          {!display ? (
            <UploadImage setImage={setImage} />
          ) : (
            <Image
              src={`http://localhost:3222/file/${display}`}
              alt="Foto Paket Wisata"
              width={150}
            />
          )}
        </Form.Item>
        <Form.Item>
          <Space size={20} className={"text-end justify-end flex"}>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-softWhite text-background"
              onClick={() => router.back()}
            >
              Batal
            </button>
            <button
              className="px-4 py-2.5 text-sm font-medium rounded-xl bg-background text-softWhite"
              onClick={handleSubmit}
            >
              {mode === "edit" ? "Simpan" : "Tambah"}
            </button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
};

export default DetailPaketWisata;

DetailPaketWisata.getLayout = (page) => <CMSLayout children={page} />;
