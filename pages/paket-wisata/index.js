import React from "react";
import PaketWisataComponent from "../../components/PaketWisataComponent";
import CMSLayout from "../../layouts/CMSLayout";

const PaketWisata = () => {
  return (
    <div>
      <PaketWisataComponent />
    </div>
  );
};

export default PaketWisata;

PaketWisata.getLayout = (page) => <CMSLayout children={page} />;
