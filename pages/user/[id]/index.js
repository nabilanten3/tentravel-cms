import { message, Select } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { mutate } from "swr";
import { appConfig } from "../../../config/app";
import CMSLayout from "../../../layouts/CMSLayout";
import { userRepository } from "../../../repository/user";

const SuperAgent = require("superagent");
const { Option } = Select;

const FormUser = () => {
  const router = useRouter();
  const { id } = router.query;
  const [roleId, setRoleId] = useState();

  const { data: dataDetailUser } = userRepository.hooks.getDetailUser(id);

  const [mode, setMode] = useState("create");

  // Form Control
  const fullNameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const levelRef = useRef();
  const phoneRef = useRef();
  const addressRef = useRef();

  useEffect(() => {
    dataDetailUser ? setMode("edit") : setMode("create");
  }, [dataDetailUser]);

  const onSubmitForm = async (event) => {
    event.preventDefault();
    const oldData = dataDetailUser?.data;
    try {
      if (mode === "create") {
        const data = {
          name: fullNameRef.current.value,
          email: emailRef.current.value,
          password: passwordRef.current.value,
          level: levelRef.current.value,
          phone: phoneRef.current.value,
          address: addressRef.current.value,
        };
        await SuperAgent.post(appConfig.apiUrl + "/auth/register")
          .send(data)
          .then((res) => console.log(res));
        await mutate(userRepository.url.user());
      } else {
        const data = {
          fullname: fullNameRef.current.value
            ? fullNameRef.current.value
            : oldData.fullname,
          email: emailRef.current.value
            ? emailRef.current.value
            : oldData.email,
        };
        await SuperAgent.put(appConfig.apiUrl + `/users/${id}`)
          .send(data)
          .then((res) => console.log(res));
        await mutate(userRepository.url.user());
      }
      message
        .success(`Berhasil ${mode === "create" ? "Buat" : "Ubah"} User`)
        .then(router.push("/user"));
    } catch (e) {
      message.error("Gagal Membuat User");
      console.log(e.message);
    }
  };

  return (
    <div className="p-10 mx-8 bg-gray-100 mt-44">
      <div className="space-y-10">
        <div className="flex items-center justify-between">
          <div className="text-xl font-semibold text-background/80">
            {`${id === "create" ? "Buat" : "Ubah"} User`}
          </div>
          <button
            onClick={() => router.back()}
            className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
          >
            Back
          </button>
        </div>
        <hr className="border-gray-200" />
        <div className="bg-white/80 p-14">
          <form onSubmit={onSubmitForm}>
            {mode !== "edit" && (
              <>
                {" "}
                <label className="text-sm font-medium text-gray-500">
                  Nama
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={fullNameRef}
                    defaultValue={dataDetailUser?.data?.name}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Telepon
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={phoneRef}
                    defaultValue={dataDetailUser?.data?.name}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
                <label className="text-sm font-medium text-gray-500">
                  Alamat
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={addressRef}
                    defaultValue={dataDetailUser?.data?.name}
                    type="text"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
              </>
            )}
            <label className="text-sm font-medium text-gray-500">Level</label>
            <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
              <input
                ref={levelRef}
                defaultValue={dataDetailUser?.data?.level}
                type="text"
                required
                className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
              />
            </div>
            <label className="text-sm font-medium text-gray-500">Email</label>
            <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
              <input
                ref={emailRef}
                defaultValue={dataDetailUser?.data?.email}
                type="email"
                required
                className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
              />
            </div>
            {mode !== "edit" && (
              <div>
                <label className="text-sm font-medium text-gray-500">
                  Password
                </label>
                <div className="w-full px-4 py-2.5 mb-5 bg-gray-100 rounded-md">
                  <input
                    ref={passwordRef}
                    type="password"
                    required
                    className="w-full text-xs bg-transparent text-background/80 focus:outline-none"
                  />
                </div>
              </div>
            )}

            <div className="flex items-center justify-end mt-10">
              <button
                type="submit"
                className="px-4 py-2.5 text-sm font-medium rounded-sm bg-background text-softWhite"
              >
                {`${id === "create" ? "Buat User" : "Simpan Perubahan"}`}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default FormUser;

FormUser.getLayout = (page) => <CMSLayout children={page} />;
