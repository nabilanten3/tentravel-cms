import { message } from "antd";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useRef } from "react";
import { appConfig } from "../../config/app";
import LoginRegisterLayout from "../../layouts/LoginRegisterLayout";
import macPhoto from "../../public/assets/macPhoto.png";
import { authentication } from "../../utils/authentication";

const SuperAgent = require("superagent");

const Login = () => {
  const router = useRouter();

  // Form Control
  const emailRef = useRef();
  const passwordRef = useRef();

  const onSubmitForm = async (event) => {
    try {
      event.preventDefault();
      const data = {
        email: emailRef.current.value,
        password: passwordRef.current.value,
      };
      await SuperAgent.post(appConfig.apiUrl + "/auth/login")
        .send(data)
        .then((res) =>
          authentication.setAccessToken(res.body?.data?.access_token)
        );
      message.success("Success Logged In");
      router.push("/");
    } catch (e) {
      message.error("Failed to Login ");
      console.log(e);
    }
  };
  return (
    <div className="flex items-center justify-center w-full h-full bg-gray-200">
      <div className=" bg-gray-100 rounded-lg p-12 ">
        <div className="flex flex-col gap-10">
          <div className="flex flex-col gap-y-2">
            <h1 className=" text-3xl font-bold tracking-widest">TENTRAVEL</h1>
            <div>
              don't have an account?
              <span>
                <Link className="" href={"/auth/register"}>
                  {" "}
                  Sign Up
                </Link>
              </span>
            </div>
          </div>
          <div className="w-full">
            <form onSubmit={onSubmitForm} className="space-y-2 xl:space-y-4">
              <div className="w-full rounded-xl">
                <label>Email</label>
                <input
                  ref={emailRef}
                  type="email"
                  required
                  className="w-full h-[45px] rounded-xl "
                />
              </div>
              <div className="w-full rounded-2xl">
                <label>Password</label>
                <input
                  ref={passwordRef}
                  type="password"
                  required
                  className="w-full h-[45px] rounded-xl"
                />
              </div>
              <div className="py-2">
                <button className="w-full h-[45px] bg-[#262528] text-white rounded-xl">
                  Sign In
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;

Login.getLayout = (page) => (
  <LoginRegisterLayout title="Encrease - Login" children={page} />
);
