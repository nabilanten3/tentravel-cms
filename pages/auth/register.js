import { Button, Form, Input, message, Select } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useRef, useState } from "react";
import { appConfig } from "../../config/app";
import LoginRegisterLayout from "../../layouts/LoginRegisterLayout";

const SuperAgent = require("superagent");

const Register = () => {
  const router = useRouter();
  const [form] = Form.useForm();

  const handleSubmit = async () => {
    const values = await form.validateFields();
    console.log(values, "values");
    try {
      await SuperAgent.post(appConfig.apiUrl + "/auth/register")
        .send(values)
        .then((res) => console.log(res));
      message.success("Success Register");
      router.push("/auth/login");
    } catch (e) {
      message.error("Failed Register ");
    }
  };

  return (
    <div className="flex items-center justify-center w-full h-full bg-gray-200">
      <div className=" bg-gray-100 rounded-lg p-12 ">
        <div className="flex flex-col gap-10">
          <div className="flex flex-col gap-y-2">
            <h1 className=" text-3xl font-bold tracking-widest">TENTRAVEL</h1>
            <div>
              Already have an account?
              <span>
                <Link className="" href={"/auth/login"}>
                  {" "}
                  Sign In
                </Link>
              </span>
            </div>
          </div>
          <div className="w-full">
            <Form layout="vertical" form={form}>
              <Form.Item name={"name"} label={"name"}>
                <Input />
              </Form.Item>
              <Form.Item name={"email"} label={"email"}>
                <Input />
              </Form.Item>
              <Form.Item name={"phone"} label={"phone"}>
                <Input />
              </Form.Item>
              <Form.Item name={"address"} label={"address"}>
                <Input.TextArea />
              </Form.Item>
              <Form.Item name={"password"} label={"password"}>
                <Input.Password />
              </Form.Item>
              <Form.Item name={"level"} label={"level"}>
                <Select
                  required
                  className="w-full h-[45px] rounded-xl "
                  options={[
                    {
                      value: "pemilik",
                      label: "pemilik",
                    },
                    {
                      value: "admin",
                      label: "admin",
                    },
                    {
                      value: "operator",
                      label: "operator",
                    },
                    {
                      value: "Pelanggan",
                      label: "pelanggan",
                    },
                  ]}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  block
                  type="text"
                  className="bg-background text-softWhite"
                  onClick={handleSubmit}
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;

Register.getLayout = (page) => (
  <LoginRegisterLayout title="Encrease - Register" children={page} />
);
