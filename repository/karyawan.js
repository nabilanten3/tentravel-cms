import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  karyawan: () => "/karyawan?limit=100",
  detailkaryawan: (id) => `/karyawan/${id}`,
};

const hooks = {
  usekaryawan() {
    return useSwr(url.karyawan(), http.get);
  },
  useDetailkaryawan(id) {
    return useSwr(url.detailkaryawan(id), http.get);
  },
};

const api = {
  createkaryawan(data) {
    return http.post(url.karyawan(), data);
  },
  updatekaryawan(id, data) {
    return http.put(url.detailkaryawan(id), data);
  },
  deletekaryawan(id) {
    return http.del(url.detailkaryawan(id));
  },
};

export const karyawanRepository = { url, hooks, api };
