import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  user: () => `/users?limit=100`,
  detailUser: (id) => `/users/${id}`,
  createUser: () => "/auth/register",
};

const hooks = {
  getUser() {
    return useSwr(url.user(), http.get);
  },
  getDetailUser(id) {
    return useSwr(url.detailUser(id), http.get);
  },
};

const api = {
  createUser: (data) => {
    return http.post(url.createUser()).send(data);
  },
  updateUser: (id, data) => {
    return http.put(url.detailUser(id)).send(data);
  },
};

export const userRepository = { url, hooks, api };
