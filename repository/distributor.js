import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  distributor: (page) => `/distributor`,
  detailDistributor: (id) => `/distributor/${id}`,
};

const hooks = {
  getDistributor(page) {
    return useSwr(url.distributor(page), http.get);
  },
  getDetailDistributor(id) {
    return useSwr(url.detailDistributor(id), http.get);
  },
};

const api = {
  createDistributor(data) {
    return http.post(url.distributor(), data);
  },
  updateDistributor: (id, data) => {
    return http.put(url.detailDistributor(id)).send(data);
  },
};

export const distributorRepository = { url, hooks, api };
