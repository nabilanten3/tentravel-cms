import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  penjualan: () => "/penjualan",
  detailPenjualan: (id) => `/penjualan/${id}`,
};

const hooks = {
  usePenjualan() {
    return useSwr(url.penjualan(), http.get);
  },
  useDetailPenjualan(id) {
    return useSwr(url.detailPenjualan(id), http.get);
  },
};

const api = {
  createPenjualan(data) {
    return http.post(url.penjualan(), data);
  },
  updatePenjualan(id, data) {
    return http.put(url.detailPenjualan(id), data);
  },
  deletePenjualan(id) {
    return http.del(url.detailPenjualan(id));
  },
};

export const penjualanRepository = { url, hooks, api };
