import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  pembelian: () => "/pembelian",
  detailpembelian: (id) => `/pembelian/${id}`,
};

const hooks = {
  usepembelian() {
    return useSwr(url.pembelian(), http.get);
  },
  useDetailpembelian(id) {
    return useSwr(url.detailpembelian(id), http.get);
  },
};

const api = {
  createpembelian(data) {
    return http.post(url.pembelian(), data);
  },
  updatepembelian(id, data) {
    return http.put(url.detailpembelian(id), data);
  },
  deletepembelian(id) {
    return http.del(url.detailpembelian(id));
  },
};

export const pembelianRepository = { url, hooks, api };
