import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  paketWisata: () => "/products/paket-wisata",
  detailpaketWisata: (id) => `/products/paket-wisata/${id}`,
};

const hooks = {
  usepaketWisata() {
    return useSwr(url.paketWisata(), http.get);
  },
  useDetailpaketWisata(id) {
    return useSwr(url.detailpaketWisata(id), http.get);
  },
};

const api = {
  createpaketWisata(data) {
    return http.post(url.paketWisata(), data);
  },
  updatepaketWisata(id, data) {
    return http.put(url.detailpaketWisata(id), data);
  },
  deletepaketWisata(id) {
    return http.del(url.detailpaketWisata(id));
  },
};

export const paketWisataRepository = { url, hooks, api };
