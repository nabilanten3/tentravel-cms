import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  reservasi: () => "/reservations",
  detailreservasi: (id) => `/reservations/${id}`,
  detailreservasiDelete: (id) => `/reservations/paket-wisata/${id}`,
};

const hooks = {
  usereservasi() {
    return useSwr(url.reservasi(), http.get);
  },
  useDetailreservasi(id) {
    return useSwr(url.detailreservasi(id), http.get);
  },
};

const api = {
  createreservasi(data) {
    return http.post(url.reservasi(), data);
  },
  updatereservasi(id, data) {
    return http.put(url.detailreservasi(id), data);
  },
  deletereservasi(id) {
    return http.del(url.detailreservasi(id));
  },
};

export const reservasiRepository = { url, hooks, api };
